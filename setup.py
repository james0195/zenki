from setuptools import setup

setup(
    name='zenki',
    packages=['zenki'],
    include_package_data=True,
    install_requires=[
        'flask'
    ],
)