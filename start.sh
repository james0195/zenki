#!/bin/bash
export FLASK_APP=zenki
export FLASK_DEBUG=true

db_file=./zenki/app.db

if [ ! -e "$db_file" ]; then
  # This script was created in zenki/zenki.py by the "initdb_command" method.
  flask initdb
fi

flask run --host=0.0.0.0 --port=5000
