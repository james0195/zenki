(function($){
  $.fn.extend({
      animateCss: function (animationName, callback) {
          var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
          this.addClass('animated ' + animationName).one(animationEnd, function() {
              $(this).removeClass('animated ' + animationName);
              console.log("animate css called")
              if (callback) {
                callback();
              }
          });
          return this;
      }
  });
})(jQuery)