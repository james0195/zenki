-- DROP TABLE users;
CREATE TABLE IF NOT EXISTS users (
  id integer primary key autoincrement,
  -- id Gid not null,
  email character(30) not null,
  username character(20) not null,
  password blob not null
);

-- DROP TABLE tasks;
CREATE TABLE IF NOT EXISTS tasks (
  id integer primary key autoincrement,
  -- id Gid not null,
  title character(50) not null,
  'text' character(150) not null,
  completed bigint not null default 0,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);

-- DROP TABLE user_tasks;
CREATE TABLE IF NOT EXISTS user_tasks (
  id integer primary key autoincrement,
  user_id integer not null,
  task_id integer not null,
  FOREIGN KEY(user_id) REFERENCES users(user_id),
  FOREIGN KEY(task_id) REFERENCES tasks(task_id)
);
