import os
import sqlite3
import click
import itertools
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
# from flask_login import LoginManager
from passlib.hash import pbkdf2_sha256

# Create the application instance
app = Flask(__name__)
# Load config from this file
app.config.from_object(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'app.db'),
    # This key is used to keep client side sessions secure. make it secure
    SECRET_KEY='dev key',
    EMAIL='test@zenki.com',
    PASSWORD='qweasd@@!#'
))

app.config.from_envvar('APP_SETTINGS', silent=True)

# # login manager from flask-login
# login_manager = LoginManager()
# # Init app into login_manager
# login_manager.init_app(app)
#
# @login_manager.user_loader
# def load_user(user_id):
#     return User.get(user_id)

def hashPassword(password):
    return pbkdf2_sha256.encrypt(password, rounds=200000, salt_size=16);

def verifyPassword(password, passwordHash):
    return pbkdf2_sha256.verify(password, passwordHash)

# source: https://stackoverflow.com/a/18842491

def connect_db():
    """ Connects to a specified database """
    con = sqlite3.connect(app.config['DATABASE'])
    con.row_factory = sqlite3.Row
    return con

# Initiliaze database
def init_db():
    # Get db for current context
    db = get_db()
    # Open our schema file
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

# Temp user Logic
def getProfile(key, value):
    db = get_db()
    cur = db.execute('SELECT id, email, username, password FROM users WHERE '+ key +' = ?', [value])
    return cur.fetchone()

def create_user(email, username, password):
    email = str(email).lower()
    username = str(username).lower()
    password = password

    error = ''

    profile = getProfile('email', email)

    try:
        if profile is not None:
            if profile['email'] == email:
                error = "Email already exist"
            if profile['username'] == username:
                error = "Username already exist"
            raise Exception(error);

        db = get_db()
        c = db.execute('INSERT INTO users (email, username, password) VALUES (?, ?, ?)', [email, username, hashPassword(password)])
        db.commit()

        print("user created!", c.lastrowid)

        return getProfile('email', email)
    except Exception as e:
        print("Create User Error:", e)
        raise e


@app.cli.command('adduser')
@click.option('--email')
@click.option('--username')
@click.option('--password')
def adduser_command(email, username, password):
    print("Create user", email, username, password)
    profile = create_user(email, username, password)

# This decorator provides way to access flask context without requesting
@app.cli.command('initdb')
def initdb_command():
    init_db()
    print("Initiliazed the db.")

# This serves to serve a single db connection per app context.
def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

# For every flask app a teardown_appcontext decorator will execute at the end of a request.
@app.teardown_appcontext
def close_db(err):
    # We want to close our db connection if it exist in current context
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

# App Routes

# Root Route
@app.route('/')
def landing():
    return render_template('landing.html.j2', title='Home')

# tasks Route
def getUserTasks(condition='', values=[]):
    db = get_db()

    print('test', condition, values)

    # Select all tasks newest to oldest
    cur = db.execute('''
        SELECT  ut.id,
                t.id as task_id,
                u.id as owner_id,
                u.username as owner_name,
                t.title,
                t.text,
                t.completed,
                t.timestamp
        FROM user_tasks ut
            INNER JOIN users u
            ON u.id = ut.user_id
            INNER JOIN tasks t
                ON t.id = ut.task_id
        ''' + condition, values)

    # Finally fetch all selections to our tasks variable
    tasks = cur.fetchall()
    if tasks is None: tasks = []

    return tasks

def getTasks(condition='', values=[]):
    tasks = getUserTasks(condition, values)
    return tasks

@app.route('/tasks', defaults={'filter': 'all'})
@app.route('/tasks/<filter>')
def tasks(filter):
    """ Show Zenki tasks """

    condition = ''
    values = []

    if filter == 'completed':
        condition = 'WHERE completed > ?'
        values = [0]

    elif filter == 'todo':
        condition = 'WHERE completed == ?'
        values = [0]

    condition = condition + 'ORDER BY timestamp DESC LIMIT 25'

    tasks = getTasks(condition, values)

    # Render our todos template with tasks passed in as a context
    return render_template('tasks.html.j2', filter=filter, title='Todos', tasks=tasks)


@app.route('/my-tasks', defaults={'filter': 'all'})
@app.route('/my-tasks/<filter>')
def my_tasks(filter):
    """ Show Zenki tasks """
    # Get a db connection
    db = get_db()

    condition = 'WHERE ut.user_id = ?'
    values = [session.get('user_id')]

    if filter == 'completed':
        condition = condition + ' AND t.completed > ?'
        values = values + [0]

    elif filter == 'todo':
        condition = condition + ' AND t.completed == ?'
        values = values + [0]

    condition = condition + 'ORDER BY timestamp DESC'


    tasks = getUserTasks(condition, values)

    # Render our todos template with tasks passed in as a context
    return render_template('tasks.html.j2', filter=filter, title='Todos', tasks=tasks)

@app.route('/toggle_task/<id>')
def toggle_task(id):
    if session.get('user_id') is None:
        flash("Interested huh? Register or Login to show the world your acomplished goals")

    else:
        completion = 0
        db = get_db()

        cur = db.execute('SELECT id, completed FROM tasks WHERE id = ?', [id])

        task = cur.fetchone()

        if task is not None:

            cur = db.execute('SELECT id FROM user_tasks WHERE user_id = ? AND task_id = ?', [session.get('user_id'), task['id']])

            user_task = cur.fetchone()

            if user_task is not None:
                if task['completed'] > 0:
                    completion = 0
                else:
                    completion = 1

                db.execute('UPDATE tasks SET completed = ? WHERE id = ?', [completion, id])
                db.commit()
            else:
                flash("Clicking on your own task will toggle its status")

    return redirect(request.referrer)

# Add Route
@app.route('/add', methods=['POST'])
def add_task():
    # If the user is not logged in
    if not session.get('logged_in') or not session.get('user_id'):
        # Abort request and give a un authorized signal (401)
        return redirect(url_for('logout'))

    # dont feel like typing the full request var each time lol
    req = request
    # as a lazy programmer I would hate to dot notate if I don't have to
    form = req.form
    # Get db connection from context
    db = get_db()
    # Insert our tasks using forms title and text
    cur = db.execute('INSERT INTO tasks (title, text) VALUES (?, ?)', [form['title'], form['text']])

    db.execute('INSERT INTO user_tasks (user_id, task_id) VALUES (?, ?)', [session.get('user_id'), cur.lastrowid])

    # If nothing happened lets commit
    db.commit()
    # Let the user know thay they have done this successfully
    flash('New task was successfully entered')

    return redirect(url_for('my_tasks'))

# Login Route
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    # dont feel like typing the full request var each time lol
    req = request
    # as a lazy programmer I would hate to dot notate if I don't have to
    # form = req.form
    # Was this a post request?
    if req.method == 'POST':
        profile = getProfile('email', request.form['email'])

        # If so check if the form email or password is not correct
        if not profile:
            error = 'Email does not exist'
        elif profile and not verifyPassword(request.form['password'], profile['password']):
            error = 'Incorrect Email Or Password'
        else:
            # If both were correct then we set a logged in session
            session['logged_in'] = True
            session['user_id'] = profile['id']

            # Greet the user
            flash('Heya, welcome back!')
            # Then finally send them to tasks route
            return redirect(url_for('tasks'))

    return render_template('login.html.j2', error=error)

# Register Route
@app.route('/register', methods=['GET', 'POST'])
def register():
    error = None
    # dont feel like typing the full request var each time lol
    req = request
    # as a lazy programmer I would hate to dot notate if I don't have to
    # form = req.form
    # Was this a post request?
    if req.method == 'POST':

        print(request.form['confirm_password'])

        if(request.form['password'] == request.form['confirm_password']):
            try:
                profile = create_user(request.form['email'], request.form['username'], request.form['password'])
                # If so check if the form email or password is not correct
                if profile:
                    # Then finally send them to tasks route
                    flash("Successfully Registered! You may now login.")
                    return redirect(url_for('login'))
            except Exception as e:
                error = e
        else:
            error = "Passwords Do Not Match"

    return render_template('register.html.j2', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('user_id', None)

    flash('Successfully Logged Out')
    return redirect(url_for('landing'))
